  resource "aws_db_instance" "student" {
  identifier           = "studentapp-tester27-rds"
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "studentapp"
  username             = "student"
  password             = "password"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = "true"
  }


resource "null_resource" "schema"{
 
    provisioner "local-exec" {
         command = <<EOT
         curl -s https://manojdevops9.s3.amazonaws.com/terraform/studentapp-ui-proj.sql >/tmp/student.sql;
         mysql -h ${aws_db_instance.student.address} -u student -ppassword </tmp/student.sql 
         EOT
         } 
}


