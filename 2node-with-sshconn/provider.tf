provider "aws" {
     region  = "us-east-1"
   }

  terraform {
  backend "s3" {
    bucket = "manojdevops9"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}