  resource "aws_instance" "terraformsv9" {
  ami                     = "ami-042d4454b8b4a0048"
  instance_type           = "t2.micro"
  availability_zone       = "us-east-1b"
  subnet_id               = "subnet-1e2b7530"
  vpc_security_group_ids  = [ "sg-00122cdb88a551c1d" ]
  key_name                = "ManojKeyLatest"
  iam_instance_profile    = "ec2_s3_byManu"
 
  tags = {
      Name          = "ManojDevops27"
      CreateBy      = "CareerIT"
      Env           = "Tester"
      Use           = "DevOpsTeamManoj"
      key           = "ManojKeyLatest"
         }
 provisioner "file" {
              connection {
               type = "ssh"
               user = "centos"
               private_key = file("/var/lib/jenkins/.ssh/id_rsa")
               host = self.public_ip
              }
              source      = "/var/lib/jenkins/.ssh/id_rsa"
              destination = "/home/centos/.ssh/id_rsa"
          }   
          
 provisioner "remote-exec"{
      connection {
        type = "ssh"
        user = "centos"
        private_key = file("/var/lib/jenkins/.ssh/id_rsa")
        host = self.public_ip
           }
            inline = [
            "sudo yum install ansible git -y",
            "echo -e 'Host *\n\t StrictHostKeyChecking no' > /home/centos/.ssh/config" ,
            "chmod 700 /home/centos/.ssh/id_rsa /home/centos/.ssh/config",
            "ansible-pull -U git@gitlab.com:SanthoshRamana/myawesomeproject.git Ansible/stack1.yml -e DBUSERNAME=student -e DBPASSWORD=password -e DBNAME=studentapp -e DBENDPOINT=${aws_db_instance.student.address}"
            ]     
         }
     }
resource "local_file" "foo" {
    content     = "${aws_instance.terraformsv9.private_ip}"
    filename = "/tmp/hosts-fromterraform"
}
resource "local_file" "public-ip" {
    content     = "${aws_instance.terraformsv9.public_ip}"
    filename = "/tmp/publichosts-fromterraform"
}

     
      