resource "aws_security_group" "opex" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"

   ingress {

    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks     = ["0.0.0.0/0"]

  }

   
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "TCP"
    cidr_blocks     = ["0.0.0.0/0"]
    
  }
 
  
}

output "security_group_id" {
  value = " ID = ${aws_security_group.opex.id} , OWNER = ${aws_security_group.opex.owner_id}" 
}