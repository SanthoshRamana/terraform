module "mngt" {
  source = "./mngt"
  Mgmt_VPC_CIDR = "${var.Mgmt_VPC_CIDR}"
}

module "staging" {
  source = "./staging"
  Stg_VPC_CIDR = "${var.Stg_VPC_CIDR}"
  OWNER = "${data.aws_caller_identity.current.account_id}"
  DEFAULT_VPC_CIDR = "172.31.0.0/16"

  
  
}


 
