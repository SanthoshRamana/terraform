
resource "aws_instance" "app" {
  count                   = "2"
  ami                     = "${var.ami}"
  instance_type           = "t2.micro"
  subnet_id               = "${tolist(data.aws_subnet_ids.subnetss.ids)[count.index]}"
  vpc_security_group_ids  = ["sg-0486a8be8e6b798d1"]
  key_name                = "ManojKeyLatest"

   tags = {
    Proj= "Student"
    Env  = "Prod"
    Name  = "AppServer-${var.App_Version}"
  }


  
provisioner "file" {
              connection {
               type = "ssh"
               user = "centos"
               private_key = file("/var/lib/jenkins/.ssh/id_rsa")
               host = self.public_ip
               }
              source      = "/var/lib/jenkins/.ssh/id_rsa"
              destination = "/home/centos/.ssh/id_rsa"
          }   

 provisioner "remote-exec"{
      connection {
        type = "ssh"
        user = "centos"
        private_key = file("/var/lib/jenkins/.ssh/id_rsa")
        host = self.public_ip
           }
            inline = [
            "sudo yum install ansible git -y",
            "echo -e 'Host *\n\t StrictHostKeyChecking no' > /home/centos/.ssh/config" ,
            "chmod 700 /home/centos/.ssh/id_rsa /home/centos/.ssh/config",
            "ansible-pull -U git@gitlab.com:SanthoshRamana/myawesomeproject.git Ansible/stack1.yml -e DBUSERNAME=student -e DBPASSWORD=password -e DBNAME=studentapp -e DBENDPOINT=dummy"
            ]     
         }

}