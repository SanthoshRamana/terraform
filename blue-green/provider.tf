provider "aws" {}

terraform {
  backend "s3" {
    bucket = "manojdevops9"
    key    = "blue-green/terraform.tfstate"
    region = "us-east-1"
  }
}