resource "aws_elb" "app-elb" {
  name               = "studentapp-elb-${var.App_Version}"
  availability_zones = "${data.aws_availability_zones.available.names}"

  listener {
    instance_port     = 800
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = "${aws_instance.app.*.id}"
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "Student-Elb-${var.App_Version}"
  }
}