  resource "aws_instance" "terraformstu26" {
  ami                     = "ami-042d4454b8b4a0048"
  instance_type           = "t2.micro"
  subnet_id               = "${aws_subnet.pub-subnets.0.id}"
  vpc_security_group_ids  =  [ "${aws_security_group.ec2-sg.id}" ]
  
 
  tags = {
      Name          = "ManojDevops26-stg"
      CreateBy      = "CareerIT"
      Env           = "Tester"
      Use           = "DevOpsTeamManoj"
      key           = "ManojKeyLatest"
         }

  }   


resource "null_resource"  "setting-up-app" {  
   provisioner "file" {
              connection {
               type = "ssh"
               user = "centos"
               private_key = file("/var/lib/jenkins/.ssh/id_rsa")
               host = "${aws_instance.terraformstu26.public_ip}"
              }
              source      = "/var/lib/jenkins/.ssh/id_rsa"
              destination = "/home/centos/.ssh/id_rsa"
          }   
  
         
 provisioner "remote-exec"{
      connection {
        type = "ssh"
        user = "centos"
        private_key = file("/var/lib/jenkins/.ssh/id_rsa")
        host = "${aws_instance.terraformstu26.public_ip}" 
           }
            inline = [
            "sudo yum install ansible git -y",
            "echo -e 'Host *\n\t StrictHostKeyChecking no' > /home/centos/.ssh/config" ,
            "chmod 700 /home/centos/.ssh/id_rsa /home/centos/.ssh/config",
            "ansible-pull -U git@gitlab.com:SanthoshRamana/myawesomeproject.git Ansible/stack1.yml"
            ]     
         }
     }

