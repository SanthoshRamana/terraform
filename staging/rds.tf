resource "aws_db_instance" "stage-rds-db" {
  allocated_storage           = 20
  storage_type                = "gp2"
  engine                      = "mariadb"
  engine_version              = "10.3"
  instance_class              = "db.t2.micro"
  name                        = "studentapp"
  username                    = "student"
  password                    = "password1"
  parameter_group_name        = "default.mariadb10.3"
  identifier                  = "studentapp-db-staging"
  skip_final_snapshot         =  "true"
  db_subnet_group_name        = "${aws_db_subnet_group.database.name}"
  vpc_security_group_ids      =  [ "${aws_security_group.ec2-sg.id}" ]
 
}

resource "null_resource" "schema-setup" {
  provisioner "local-exec"{
    command = <<EOF
      curl -s https://manojdevops9.s3.amazonaws.com/studentapp-ui-project.sql  > /tmp/student.sql
      mysql -h "${aws_db_instance.stage-rds-db.address}" -u student -ppassword1
    EOF
  }
}
