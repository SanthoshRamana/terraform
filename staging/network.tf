data "aws_availability_zones" "available" {}

resource "aws_vpc" "stud-stg-vpc" {
  cidr_block       = "${var.Stg_VPC_CIDR}" 


  tags = {
    Name = "stud-stg-vpc"
  }
}


resource "aws_subnet" "pub-subnets" {
  count                       =  "${length(data.aws_availability_zones.available)}"
  vpc_id                      =  "${aws_vpc.stud-stg-vpc.id}"
  cidr_block                  =   "${cidrsubnet("${var.Stg_VPC_CIDR}", 8, count.index)}"
  availability_zone           =   "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch     =   true


  tags = {
    Name = "Stg-Pub-Subnet-${count.index+1}"
  }
}

resource "aws_subnet" "prv-subnets" {
  count                       =  "${length(data.aws_availability_zones.available)}"
  vpc_id                      =  "${aws_vpc.stud-stg-vpc.id}"
  cidr_block                  =   "${cidrsubnet("${var.Stg_VPC_CIDR}", 8, count.index+6)}"
  availability_zone           =   "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch     =   true


  tags = {
    Name = "Stg-Prv-Subnet-${count.index+1}"
  }
}




resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.stud-stg-vpc.id}"

  tags = {
    Name = "Staging-VPC"
  }
}


resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.stud-stg-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  route {
    cidr_block = "${var.DEFAULT_VPC_CIDR}"
    gateway_id = "${aws_vpc_peering_connection.myvpctostg.id}"
  }
 

  tags = {
    Name = "Stg-VPC-RT"
  }
}

resource "aws_route_table_association" "PubRTA" {
  subnet_id      = "${aws_subnet.pub-subnets.0.id}"
  route_table_id = "${aws_route_table.rt.id}"
}


resource "aws_route_table" "rt1" {
  vpc_id = "${aws_vpc.stud-stg-vpc.id}"

  route {
    cidr_block = "172.31.0.0/16"
    gateway_id = "${aws_vpc_peering_connection.myvpctostg.id}"
  }

  tags = {
    Name = "Stg-VPC-Prv-Subnet-RT"
  }
}

 resource "aws_route_table_association" "PvrRT" {
 subnet_id      = "${aws_subnet.prv-subnets.6.id}"
 route_table_id = "${aws_route_table.rt1.id}"
}
  
resource "aws_db_subnet_group" "database" {
  name       = "stage-db-subnet-group"
  subnet_ids = [ "${aws_subnet.prv-subnets.0.id}" , "${aws_subnet.prv-subnets.1.id}" , 
                   "${aws_subnet.prv-subnets.2.id}" , "${aws_subnet.prv-subnets.3.id}"]

  tags       = {
    Name     = "My DB prv subnet group for staging"
  }
}


resource "aws_vpc_peering_connection" "myvpctostg" {
  peer_owner_id = "${var.OWNER}"
  peer_vpc_id   = "${aws_vpc.stud-stg-vpc.id}"
  vpc_id        = "vpc-a8ec80d2"
  auto_accept   = true

  tags = {
    Name = "MyVPC to Stg Env Peering"
  }
}

resource "aws_route" "r" {
  route_table_id            = "rtb-0d6c8773"
  destination_cidr_block    = "${var.Stg_VPC_CIDR}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.myvpctostg.id}"
  
}