  resource "aws_instance" "web" {
  ami                     = "ami-042d4454b8b4a0048"
  instance_type           = "t2.micro"
  availability_zone       = "us-east-1b"
  subnet_id               = "subnet-1e2b7530"
  vpc_security_group_ids  = [ "sg-00122cdb88a551c1d" ]
  key_name                = "ManojKeyLatest"
 
  tags = {
      Name          = "Manojextract"
      CreateBy      = "CareerIT"
      Env           = "Tester"
      Use           = "DevOpsTeamManoj"
      key           = "ManojKeyLatest"
         }
  }   

   output "instance_ip_address" {
       value = "${aws_instance.web.private_ip}"
   }      

   resource "local_file" "foo" {
    content     = "${aws_instance.web.private_ip}!"
    filename = "/tmp/hosts-fromterraform"
}