variable "env" {}

locals {

local_prod     =  "Production"
local_nonprod  =  "Development"

  ENV = "${var.env == "prod" ? local.local_prod : local.local_nonprod}"

}

resource "aws_security_group" "sgy" {
  name        = "security-${local.ENV}"
  description = "${local.ENV}-SG"

   ingress {

    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks     = ["0.0.0.0/0"]

  }
   
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "TCP"
    cidr_blocks     = ["0.0.0.0/0"] 
     }
   
}

output "SGName"  {
  value = "${aws_security_group.sgy.name}" 
}


 