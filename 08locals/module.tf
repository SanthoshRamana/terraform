module "modulex" {
  source = "./modulex"
  env  = "${var.env}"
}

output "SecurityGroupName"  {
  value = "${module.modulex.SGName}" 
}