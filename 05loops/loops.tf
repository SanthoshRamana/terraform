data "aws_availability_zones" "available" {}
resource "aws_vpc" "stud-test-vpc" {
  cidr_block       = "10.6.0.0/16"


  tags = {
    Name = "stud-test-vpc"
  }
}

resource "aws_subnet" "subnet" {
  count              =  "${length(data.aws_availability_zones.available)}"
  vpc_id             =  "${aws_vpc.stud-test-vpc.id}"
  cidr_block         =   "${cidrsubnet("10.6.0.0/16", 8, count.index)}"
  availability_zone  = "${data.aws_availability_zones.available.names[count.index]}"


  tags = {
    Name = "Manoj"
  }
}


