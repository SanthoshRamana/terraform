
variable  "sgid" {}

resource "aws_instance" "modtest" {
  ami                     = "ami-042d4454b8b4a0048"
  instance_type           = "t2.micro"
  availability_zone       = "us-east-1a"
  subnet_id               = "subnet-d1feaeb6"
  vpc_security_group_ids  = [ "${var.sgid}" ]
  key_name                = "ManojKeyLatest"
  
}


resource "aws_instance" "modprod" {
  ami                     = "ami-042d4454b8b4a0048"
  instance_type           = "t2.nano"
  availability_zone       = "us-east-1a"
  subnet_id               = "subnet-d1feaeb6"
  vpc_security_group_ids  = [ "${var.sgid}" ]
  key_name                = "ManojKeyLatest"
}

output "IPtest"  {
  value = "TestIP = ${aws_instance.modtest.public_ip}" 
}
  
output "IPprod"  {
  value = "ProdIP = ${aws_instance.modprod.public_ip}" 
}