resource "aws_security_group" "sgx" {
  name        = "securitygrpx"
  description = "allows ports for ssh"

   ingress {

    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks     = ["0.0.0.0/0"]

  }
   
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "TCP"
    cidr_blocks     = ["0.0.0.0/0"] 
     }
   
}

output "sgid"  {
  value = "${aws_security_group.sgx.id}" 
}


 