module "modulex" {
  source = "./modulex"
}

module "moduley" {
  source = "./moduley"
  sgid   = "${module.modulex.sgid}"
}

output "Prod_Public_Ip"  {
  value = "Test_IP = ${module.moduley.IPtest}" 
}

output "Test_Public_Ip"  {
  value = "Prod_IP = ${module.moduley.IPprod}" 
}