resource "aws_security_group" "ec2-sg" {
  name                    = "EC2-SG"
  description             = "Allow TLS inbound traffic"
  vpc_id                  = "${aws_vpc.stud-stg-vpc.id}"
  

  ingress {

    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks     = ["0.0.0.0/0"]

  }

    ingress {

    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0" , "172.31.0.0/16" , "10.6.0.0/16" ]

  }

    ingress {

    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]

  }

     ingress {

    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    
  }
}


