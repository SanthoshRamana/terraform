data "aws_availability_zones" "available" {}
data "aws_caller_identity" "current" {}
data  aws_availability_zones availables {}


data "aws_ami" "centos" {
   
   owners            = "{data.aws_caller_identity.current.account_id}"
   most_recent      = true
 
   filter {
    name   = "name"
    values = ["centosu*"]
  }
}

