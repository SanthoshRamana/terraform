data "aws_availability_zones" "available" {}

resource "aws_vpc" "stud-mngt-vpc" {
  cidr_block       = "${var.Mgmt_VPC_CIDR}"


  tags = {
    Name = "stud-mngt-vpc"
  }
}


resource "aws_subnet" "subnets" {
  count              =  "${length(data.aws_availability_zones.available)}"
  vpc_id             =  "${aws_vpc.stud-mngt-vpc.id}"
  cidr_block         =   "${cidrsubnet("${var.Mgmt_VPC_CIDR}", 8, count.index)}"
  availability_zone  =   "${data.aws_availability_zones.available.names[count.index]}"


  tags = {
    Name = "Subnet-${count.index+1}"
  }
}