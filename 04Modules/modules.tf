module "Management" {
  source = "./Management"
}

module "Prod" {
  source = "./Prod"
}

module "Stag" {
  source = "./Stag"
}

module "DemoOnVariables" {
  source = "./DemoOnVariables"
  VPC_CIDR = "${var.VPC_CIDR}"
}
