resource "aws_vpc" "mngt-vpc" {
  cidr_block       = "10.0.0.0/16"


  tags = {
    Name = "Management VPC"
  }
}

resource "aws_subnet" "subnet1" {
  vpc_id     = "${aws_vpc.mngt-vpc.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"


  tags = {
    Name = "Subnet1"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id     = "${aws_vpc.mngt-vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "Subnet2"
  }
}

resource "aws_subnet" "subnet3" {
  vpc_id     = "${aws_vpc.mngt-vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "Subnet3"
  }
}

resource "aws_subnet" "subnet4" {
  vpc_id     = "${aws_vpc.mngt-vpc.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "us-east-1d"

  tags = {
    Name = "Subnet3"
  }
}