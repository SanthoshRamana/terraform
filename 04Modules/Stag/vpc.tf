resource "aws_vpc" "stag-vpc" {
  cidr_block       = "10.2.0.0/16"


  tags = {
    Name = "Stag VPC"
  }
}